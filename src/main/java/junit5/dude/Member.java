package junit5.dude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Member {
    private String firstName;
    private String middleName;
    private String lastName;
    private Address address;

    public String toString() {
        return firstName + ' ' + middleName + ' ' + lastName;
    }
}
