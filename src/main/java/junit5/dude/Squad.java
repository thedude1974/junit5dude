package junit5.dude;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Squad {
    private String name;
//    private Address address;
    private List<Member> members = new ArrayList<>();

    public void addMember(Member member) {
        members.add(member);
    }

    public Member searchMember(Member member) {
        return members.stream()
                .filter(p -> p.equals(member))
                .findFirst()
                .orElse(null);
    }

    public Boolean memberExists(Member member) {
        return members.contains(member);
    }
}
