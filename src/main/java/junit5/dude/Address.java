package junit5.dude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Address {
    private String Street;
    private String Housenumber;
    private String ZipCode;
    private String City;
}
