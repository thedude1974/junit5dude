package junit5.dude;

public class Main {
    public static void main(String[] args) {
        createNewSquad();
    }

    public static void createNewSquad() {
        Squad squad = new Squad();
        squad.addMember(new Member("Doede", "", "Wind", new Address("Boomkensdiep", "3", "8602CP", "Sneek")));
        squad.addMember(new Member("Laurens", "", "Pool", new Address("Laurenslaan", "1", "1234AB", "Bakkeveen")));
        squad.addMember(new Member("Wietse", "", "Dijkstra", new Address("Vogelzwin", "26", "8602ZZ", "Sneek")));
        squad.addMember(new Member("Maarten", "", "Bron", new Address("Bronstraat", "99", "4444AA", "Groningen")));
        squad.addMember(new Member("Bert", "", "Tiemersma", new Address("Bildtsestraat", "71", "8913AA", "Leeuwarden")));

        squad.getMembers().stream()
//                .filter(member -> member.toString().contains("oo"))
                .forEach(member -> System.out.println(member));
    }
}
