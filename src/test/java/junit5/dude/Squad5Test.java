package junit5.dude;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;


class Squad5Test {
    private static Squad squad;
    private static Member memberDoede, memberLaurens;

    @BeforeAll
    public static void init() {
        squad = new Squad();
        memberDoede = new Member();
        memberDoede.setFirstName("Doede");
        memberDoede.setLastName("Wind");
        memberLaurens = new Member("Laurens", "", "Pool", new Address("Laurenslaan", "1", "1234AB", "Bakkeveen"));
    }

    @Test
    @DisplayName("Toevoegen members aan squad")
    public void addMember() {
        squad.addMember(memberDoede);
        squad.addMember(memberLaurens);
        Member memberSakeKampen = new Member("Sake", "", "Kampen", new Address("Kampenweg", "10", "4321AB", "Drachten"));
        assumeTrue(squad.getMembers().size() == 2);

        assertAll(
                () -> assertFalse(squad.memberExists(memberSakeKampen)),
                () -> assertNotEquals(squad.searchMember(memberSakeKampen), memberSakeKampen, "Member mag niet gevonden worden."),
                () -> assertTrue(squad.memberExists(memberDoede)),
                () -> assertTrue(squad.memberExists(memberLaurens)),
                () -> assertEquals(squad.searchMember(memberLaurens), memberLaurens),
                () -> assertTrue(squad.getMembers().size() == 2, "Member size niet juist.")
        );
    }
}
